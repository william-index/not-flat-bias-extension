## Sway
### Huge Change Hackathon Project  
#### Jan 13 — Jan 14, 2017

![thinkprogress-article](screenshots/thinkprogress-article.png)

Individuals, increasingly subjected to biased and fake news, have been easily convinced of xenophobic pronouncements along ethnic/racial, gender, and class lines.

Sway is a chrome extension that identifies (through machine learning) bias within news articles and makes clear the degree of bias presented and to what end of the political spectrum. Our hope is that clarifying that articles lean towards an extreme bias could aid in reducing de-facto belief in said xenophobic statements.

After an article is identified, a websearch is run to find similar articles that are then curated to show opposing views. These views can be used to either break from ones political bubble, or to understand the counter-argument to further cement one's own.

## Project Team:  
### BJ Warshaw
#### FE Engineer

### Cristina Shin
#### Designer

### Ryan Balch
#### BE Engineer/Dataset refinement

### William Anderson
#### FE Engineer
