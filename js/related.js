var BIAS = BIAS || {};

var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

BIAS.escapeHtml = function (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
};

BIAS.relatedLinksChunk = 5;
BIAS.relatedLinksOffset = 1;
BIAS.relatedLinksTotalNeeded = 3;
BIAS.allRelatedLinks = [];
BIAS.relatedLinksToRender = [];
BIAS.numRequests = 0;
BIAS.maxBingRequests = 1;
BIAS.bingResults = 0;

BIAS.getRelatedLinks = function() {
  var terms = BIAS.getSearchTerms();

  // console.log('Get related: ' + terms);

  BIAS.relatedLinksCount = 1;
  BIAS.relatedLinksData = [];

  var params = {
    q: terms,
    count: '' + BIAS.relatedLinksChunk,
    offset: '' + BIAS.relatedLinksOffset,
    mkt: 'en-us',
    safeSearch: 'Moderate'
  };

  BIAS.numRequests++;

  $.ajax({
    url: "https://api.cognitive.microsoft.com/bing/v5.0/news/search?" + $.param(params),
    type: 'GET',
    beforeSend: function(request) {
      request.setRequestHeader('Ocp-Apim-Subscription-Key', '2cef52489455473da47d3a786a73bc94');
    },
    success: function (response) {
      console.log('Bing success', response, response.value.length);
      BIAS.bingResults = response.value.length;
      BIAS.determineBiasForLinks(response.value);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr.status);
      console.log(thrownError);
    }
  });
};

BIAS.determineBiasForLinks = function(links) {
  var linkFetching = [];

  console.log('Determining related link bias');

  for (var i = 1; i < links.length; i++) {
    var linkAjax = BIAS.getBiasFromUrl(links[i].url);

    linkAjax.then(function(response) {
      BIAS.relatedLinksCount++;

      console.log('Bias success for related link', response);
      // console.log(BIAS.relatedLinksCount, BIAS.relatedLinksChunk);

      if (BIAS.relatedLinksCount == BIAS.bingResults) {
        // all biases retrieved
        BIAS.filterRelatedLinks();
      }
    });

    BIAS.relatedLinksData.push({
      ajax: linkAjax,
      data: links[i]
    });
  }
};

BIAS.filterRelatedLinks = function() {
  var currentBias = BIAS.pageBiasResults.label;
  var targetBias = [
    'Liberal',
    'Conservative',
    'Neutral'
  ];

  console.log('Filtering related links', currentBias);

  if (currentBias == 'Liberal') {
    var index = targetBias.indexOf('Liberal');
    targetBias.splice(index, 1);
  } else if (currentBias == 'Conservative') {
    var index = targetBias.indexOf('Conservative');
    targetBias.splice(index, 1);
  }

  console.log('targetBias: ' + targetBias);

  for (var i = 0; i < BIAS.relatedLinksData.length; i++) {
    var relatedLinkData = BIAS.relatedLinksData[i];
    relatedLinkData.bias = BIAS.parseResponse(relatedLinkData.ajax.responseJSON);
    // console.log(i, relatedLinkData.bias.label, relatedLinkData.bias);

    BIAS.allRelatedLinks.push(relatedLinkData);

    if (targetBias.includes(relatedLinkData.bias.label)) {
      // console.log('Pushing to relatedLinksToRender', relatedLinkData);
      BIAS.relatedLinksToRender.push(relatedLinkData);

      if (BIAS.relatedLinksToRender.length == BIAS.relatedLinksTotalNeeded) {
        break;
      }
    }
  }

  console.log('Links to render', BIAS.relatedLinksToRender);

  if (BIAS.relatedLinksToRender.length == BIAS.relatedLinksTotalNeeded) {
    console.log('Found enough links');
    BIAS.renderRelatedLinks();
  } else if (BIAS.bingResults < BIAS.relatedLinksChunk) {
    console.log('Bing didn\'t return the full chunk: not enough results?');
    BIAS.renderRelatedLinks();
  } else {
    if (BIAS.numRequests < BIAS.maxBingRequests) {
      console.log('request');
      BIAS.relatedLinksOffset += BIAS.relatedLinksChunk;
      BIAS.getRelatedLinks();
    } else {
      console.log('stop requesting');
      BIAS.renderRelatedLinks();
    }
  }
}

BIAS.renderRelatedLinks = function() {
  // console.log('Rendering Related Links', BIAS.relatedLinksToRender.length);
  BIAS.els.alternatives.classList.add('loaded');

  if (BIAS.relatedLinksToRender.length < 3) {
    var linksNeeded = 3 - BIAS.relatedLinksToRender.length;
    for (var i = 0; i < linksNeeded; i++) {
      BIAS.relatedLinksToRender.push(BIAS.allRelatedLinks[i]);
    }
  }

  for (var i = 0; i < BIAS.relatedLinksToRender.length; i++) {
    var linkData = BIAS.relatedLinksToRender[i];
    // console.log(i, linkData);

    var articleData = {
      link: linkData.data.url,
      title: BIAS.escapeHtml(linkData.data.name),
      desc: BIAS.escapeHtml(linkData.data.description.slice(0,140)),
      // date: linkData.data.datePublished,
      name: linkData.data.provider[0].name,
      modifier: linkData.bias.modifier,
      label:  linkData.bias.label
    };

    if (linkData.data.image && linkData.data.image.thumbnail && linkData.data.image.thumbnail.contentUrl) {
      articleData.image = linkData.data.image.thumbnail.contentUrl;
    }

    var article = BIAS.createRelatedArticle(articleData);
    BIAS.els.alternatives.appendChild(article);
  }
};

BIAS.getSearchTerms = function() {
  var title = $('title').text();
  var $h1 = $('h1');
  var h1Text = '';

  $h1.each(function(i, h1) {
    var $h1 = $(h1);

    if (!$h1.closest('.not-flat').length) {
      h1Text += $(h1).text();
    }
  });

  var terms = title + ' ' + h1Text;
  console.log(terms);

  return terms;
};

BIAS.stubRelatedLinks = function() {
  BIAS.relatedLinksToRender = [
    {
      data: {
        url: 'http://motherfuckingwebsite.com',
        name: 'Lorem Ipsum Sit',
        description: 'You probably build websites and think your shit is special. You think your 13 megabyte parallax-ative home page is going to get you some fucking Awwward banner you can glue to the top corner of your site. You think your 40-pound jQuery file and 83 polyfills give IE7 a boner because it finally has box-shadow. Wrong, motherfucker. Let me describe your perfect-ass website:',
        provider: [
          {
            name: 'Motherfucking Website'
          }
        ],
        image: {
          thumbnail: {
            contentUrl: "http://i.telegraph.co.uk/multimedia/archive/03341/slowloris3_3341760b.jpg"
          }
        }
      },
      bias: {
        modifier: 'Slightly',
        label: 'Liberal'
      }
    },
    {
      data: {
        url: 'http://motherfuckingwebsite.com',
        name: 'Lorem Ipsum Sit',
        description: 'You probably build websites and think your shit is special. You think your 13 megabyte parallax-ative home page is going to get you some fucking Awwward banner you can glue to the top corner of your site. You think your 40-pound jQuery file and 83 polyfills give IE7 a boner because it finally has box-shadow. Wrong, motherfucker. Let me describe your perfect-ass website:',
        provider: [
          {
            name: 'Motherfucking Website'
          }
        ],
        image: {
          thumbnail: {
            contentUrl: "http://i.telegraph.co.uk/multimedia/archive/03341/slowloris3_3341760b.jpg"
          }
        }
      },
      bias: {
        modifier: 'Heavily',
        label: 'Conservative'
      }
    },
    {
      data: {
        url: 'http://motherfuckingwebsite.com',
        name: 'Lorem Ipsum Sit',
        description: 'You probably build websites and think your shit is special. You think your 13 megabyte parallax-ative home page is going to get you some fucking Awwward banner you can glue to the top corner of your site. You think your 40-pound jQuery file and 83 polyfills give IE7 a boner because it finally has box-shadow. Wrong, motherfucker. Let me describe your perfect-ass website:',
        provider: [
          {
            name: 'Motherfucking Website'
          }
        ],
        image: {
          thumbnail: {
            contentUrl: "http://i.telegraph.co.uk/multimedia/archive/03341/slowloris3_3341760b.jpg"
          }
        }
      },
      bias: {
        modifier: 'Heavily',
        label: 'Liberal'
      }
    }
  ];

  BIAS.renderRelatedLinks();
}
