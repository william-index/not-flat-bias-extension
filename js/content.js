var BIAS = BIAS || {};

BIAS.relatedDebug = false;

// content.js
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_browser_action" ) {
      chrome.storage.sync.get(["toggleState"], function(items){
          var state = !!items.toggleState;
          chrome.storage.sync.set({ "toggleState": !state }, function(){
            BIAS.checkToggle();
          });
      });
    }
  }
);

$(document).ready(function() {
  BIAS.checkToggle();

  BIAS.els = {};
  BIAS.injectDisplayModule(true);
  BIAS.bindEvents();

  BIAS.detectPageBias();

  if (BIAS.relatedDebug) {
    BIAS.stubRelatedLinks();
  }
});

BIAS.checkToggle = function() {
  chrome.storage.sync.get(["toggleState"], function(items){
      var state = !items.toggleState;

      if (!state) {
        document.body.classList.add('hide-bias');
      } else {
        document.body.classList.remove('hide-bias');
      }
  });
}

/**
 * Binds events to elements on the page.
 */
BIAS.bindEvents = function() {
  BIAS.els.collapseButton.addEventListener('click',
      BIAS.toggleExpansion.bind(BIAS.els.displayModule));
}

/**
 * Detect bias of current URL.
 */
BIAS.detectPageBias = function() {
  var getPageBias = BIAS.getBiasFromUrl(document.location.href);
  getPageBias.then(function(response) {
    BIAS.pageBiasResults = BIAS.parseResponse(response);

    if (BIAS.pageBiasResults.rank >= 0) {
      BIAS.setGaugeIndicator(BIAS.pageBiasResults.rank);
    } else {
      // it's other... what do we do?
    }

    // console.log('Page bias detected');
    BIAS.updateLabels();

    if (!BIAS.relatedDebug) {
      BIAS.getRelatedLinks();
    }
  });
}

/**
 * Creates the base container for the in-page expandable display.
 * @param {Bool} defaultOpen Whether to default expandable display to open
 */
BIAS.injectDisplayModule = function(defaultOpen) {
  BIAS.els.displayModule = BIAS.createExpandableDisplay(true);
  BIAS.els.biasGauge = BIAS.createBiasGauge();
  BIAS.els.indicator = BIAS.createIndicator();
  BIAS.els.label = BIAS.createLabel();
  BIAS.els.modifier = BIAS.createModifier();
  BIAS.els.moduleTitle = BIAS.createTitle();
  BIAS.els.collapseButton = BIAS.createCollapseButton();
  BIAS.els.alternatives = BIAS.createAlternativesContainer();
  BIAS.els.alternativesInner = BIAS.createAlternativesInner();
  BIAS.els.relatedTitle = BIAS.createRelatedTitle();
  BIAS.els.alternativesSpinner = BIAS.createAlternativesSpinner();

  document.body.appendChild(BIAS.els.displayModule);
  BIAS.els.displayModule.appendChild(BIAS.els.moduleTitle);
  BIAS.els.displayModule.appendChild(BIAS.els.biasGauge);
  BIAS.els.displayModule.appendChild(BIAS.els.collapseButton);
  BIAS.els.displayModule.appendChild(BIAS.els.relatedTitle);
  BIAS.els.displayModule.appendChild(BIAS.els.alternatives);
  BIAS.els.alternatives.appendChild(BIAS.els.alternativesSpinner);
  BIAS.els.alternatives.appendChild(BIAS.els.alternativesInner);
  BIAS.els.biasGauge.appendChild(BIAS.els.indicator);
  BIAS.els.biasGauge.appendChild(BIAS.els.label);
  BIAS.els.biasGauge.appendChild(BIAS.els.modifier);
}

BIAS.createRelatedTitle = function() {
  var title = "<h2 class='related-title'>Counterpoints</h2>";

  return $(title)[0];
}

BIAS.createAlternativesContainer = function() {
  var container = document.createElement("section");
      container.classList.add('not-flat__alternatives');

  return container;
}

BIAS.createAlternativesInner = function() {
  var inner = document.createElement("div");
      inner.classList.add('not-flat__alternatives-inner');

  return inner;
}

BIAS.createAlternativesSpinner = function() {
  var $spinnerContainer = $('<div class="not-flat__alternatives-spinner-container"></div>');
  var $spinner = $('<span class="not-flat__alternatives-spinner"></span>');
  var $spinnerText = $('<span class="not-flat__alternatives-text">Swaying articles...</span>');

  var spinnerImg = chrome.extension.getURL('img/loading-icon.svg');
  $spinner.css('backgroundImage', 'url(' + spinnerImg + ')');

  $spinnerContainer.append($spinner).append($spinnerText);

  return $spinnerContainer[0];
}

BIAS.createRelatedArticle = function(article) {
  // console.log('Creating related', article);
  var articleDOM  = "<a href='"+ article.link + "' class='related-article' target='_blank'>";
      // articleDOM += "<div style='background-image: url(" + article.image + ");' class='related-article__image'></div>";
      articleDOM += "<h3 class='related-article__title'>" + article.title + "</h3>";
      articleDOM += "<p class='related-article__desc'>" /* + article.date + " / " */ + article.desc + "</p>";
      articleDOM += "<span class='related-article__name'>" + article.name + "</span>";
      articleDOM += "<span class='related-article__bias " + article.modifier.toLowerCase() + " " + article.label.toLowerCase() + "'>" + article.modifier + " " + article.label + "</span>";
      articleDOM += "</a>";

  return $(articleDOM)[0];
}

BIAS.createTitle = function() {
  var title = document.createElement("h1");
      title.classList.add('not-flat__title');
      title.innerHTML = "This page is:";

  return title;
}

BIAS.createIndicator = function() {
  var indicator = document.createElement("div");
      indicator.classList.add('bias-gauge__indicator');
      indicator.classList.add('loading');

  var gaugeArrows = chrome.extension.getURL('img/gauge-arrows.svg');
  indicator.style.backgroundImage = 'url(' + gaugeArrows + ')';

  return indicator;
}

BIAS.createBiasGauge = function() {
  var biasGauge = document.createElement("div");
  var gaugeBG = chrome.extension.getURL('img/gauge.svg');

  biasGauge.classList.add('bias-gauge');
  biasGauge.style.backgroundImage = 'url(' + gaugeBG + ')';

  return biasGauge;
}

BIAS.createCollapseButton = function() {
  var collapseButton = document.createElement("div");
      collapseButton.classList.add('collapse-button');

  var collapseDOM = "<div class='collapse-button__logo'></div>";
      collapseDOM += "<p class='collapse-button__text'><span class='collapse-button__show'>Show </span><span class='collapse-button__hide-arrow'></span><span class='collapse-button__hide'>Hide </span> <span>Sway</span></p>";
  collapseButton.innerHTML = collapseDOM;

  var downArrow = chrome.extension.getURL('img/arrow-icon.svg');
      collapseButton.querySelector('.collapse-button__hide-arrow').style.backgroundImage = 'url(' + downArrow + ')';

  var expandIcon = chrome.extension.getURL('img/collapseState.svg');
      collapseButton.querySelector('.collapse-button__logo').style.backgroundImage = 'url(' + expandIcon + ')';


  return collapseButton;
}

/**
 * Sets the display position of the bias indicator.
 * @param {Number} indicatorPercent int from 0-100
 */
BIAS.setGaugeIndicator = function(indicatorPercent) {
  // console.log('setting indicator');

  var computedTransform = window.getComputedStyle(BIAS.els.indicator).getPropertyValue('transform');
      computedTransform = BIAS.getRotationFromMatrix(computedTransform);
  BIAS.els.indicator.classList.remove('loading');
  BIAS.els.indicator.style.transform = 'rotate(' + computedTransform + 'deg)';

  setTimeout(function() {
    var adjustedPercent = (indicatorPercent/100 * 210) - 105;
    BIAS.els.indicator.style.transform = 'rotate(' + adjustedPercent + 'deg)';
  }, 100);
}

BIAS.getRotationFromMatrix = function(tr) {
  var values = tr.split('(')[1];
    values = values.split(')')[0];
    values = values.split(',');
  var a = values[0];
  var b = values[1];
  var c = values[2];
  var d = values[3];

  var scale = Math.sqrt(a*a + b*b);

  // arc sin, convert from radians to degrees, round
  // DO NOT USE: see update below
  var sin = b/scale;
  var angle = Math.round(Math.asin(sin) * (180/Math.PI));
  return angle;
}


/**
 * Creates the base container for the in-page expandable display.
 * @param {Bool} defaultOpen Whether to default expandable display to open
 * @return {Object} Retrusn the container for the expandable module
 */
BIAS.createExpandableDisplay = function(defaultOpen) {
  // console.log('starting creation of display');

  var displayModule = document.createElement("section");
      displayModule.classList.add('not-flat');

  if (defaultOpen) {
    displayModule.classList.add('open');
  }

  return displayModule;
};

BIAS.createLabel = function() {
  var label = document.createElement('h1');
  label.classList.add('bias-gauge__label');

  return label;
}

BIAS.createModifier = function() {
  var label = document.createElement('h2');
  label.classList.add('bias-gauge__modifier');

  return label;
}

/**
 * Updates the bias determination label.
 */
BIAS.updateLabels = function() {
  BIAS.els.label.innerHTML = BIAS.pageBiasResults.label;
  BIAS.els.modifier.innerHTML = BIAS.pageBiasResults.modifier;

  if (BIAS.pageBiasResults.label == 'Conservative') {
    BIAS.els.label.classList.add('bias-gauge__label--conservative');
  }
};

/**
 * Toggles open class on displayModule
 * @param {Event} e
 */
BIAS.toggleExpansion = function(e) {
  this.classList.toggle('open');
}
