var BIAS = BIAS || {};

BIAS.endpoint = 'https://deep-news.appspot.com/_ah/api/bias/v1/rank';

$(document).ready(function(BIAS) {
  BIAS.getBiasFromUrl = function(url) {
    var data = {
      'url': url
    };

    return BIAS.getBias(data);
  };

  BIAS.getBiasFromText = function(text) {
    var data = {
      'text': text
    }

    return BIAS.getBias(data);
  };

  BIAS.getBias = function(data) {
    // data.override = 'conservative';
    // data.override = 'liberal';

    return $.ajax({
      url: BIAS.endpoint + "?" + $.param(data),
      type: 'POST',
      success: function (response) {
        return response;
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });
  };

  BIAS.parseResponse = function(response) {
    var o = response.other;
    var l = response.liberal;
    var c = response.conservative;
    var label = '';
    var modifier = '';
    var rank = -1;

    // console.log('Liberal: ' + l);
    // console.log('Conservative: ' + c);
    // console.log('Other: ' + o);

    if ((o > l) && (o > c)) {
      label = 'Other';
    } else {
      var total = l + c;

      // percentage conservative actually represents where, on a scale of 0-100,
      // with liberal == 0 and conservative == 100, we display our results.
      var rank = c / total * 100;
      // console.log('rank: ' + rank);

      if (rank <= 25) {
        modifier = 'Heavily';
        label = 'Liberal';
      } else if ((rank > 25) && (rank <= 45)) {
        modifier = 'Slightly';
        label = 'Liberal';
      } else if ((rank > 45) && (rank < 55)) {
        modifier = 'Likely';
        label = 'Neutral';
      } else if ((rank >= 55) && (rank < 75)) {
        modifier = 'Slightly';
        label = 'Conservative';
      } else {
        modifier = 'Heavily';
        label = 'Conservative';
      }
    }

    return {
      label: label,
      modifier: modifier,
      rank: rank
    }
  };
}(BIAS));
